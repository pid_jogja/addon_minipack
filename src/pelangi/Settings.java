
package pelangi;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;


public class Settings {
    
    private String moduleName = "";
    private String moduleTimeout = "";

    private String connectionString = "";
    private String connectionDriver = "";
    private String connectionUser = "";
    private String connectionPass = "";

    private String xmppServer = "";
    private String xmppPort = "";
    private String xmppProxy = "";
    private String xmppClient = "";
    private String xmppPin = "";

    private String xmppUsername = "";
    private String xmppPassword = "";
    private String xmppDestination = "";
    private String xmppDebug = "";
    
    private String host = "";
    private String username = "";
    private String password = "";
    private String secretKey = "";
    
    private String inquiry = "";
    private String payment = "";
    private String advice = "";
        
    private int nthreads = 1;
    private int chunk = 1;
    
    private Map<String, String> persistenceMap = new HashMap<String, String>();

    private Logger logger = Logger.getLogger(Settings.class);

    public Settings() {
        
    }
    
    public Map<String, String> getPersistenceMap() {
        return persistenceMap;
    }

    public void setPersistenceMap(Map<String, String> persistenceMap) {
        this.persistenceMap = persistenceMap;
    }
    
    public void setAppName(String url) {
        this.moduleName = url;
    }

    public String getAppName() {
        return this.moduleName;
    }
    
    public void setAppTimeout(String url) {
        this.moduleTimeout = url;
    }

    public String getAppTimeout() {
        return this.moduleTimeout;
    }

    // xmpp
    public void setXMPPDebug(String x) {
        this.xmppDebug = x;
    }

    public String getXMPPDebug() {
        return this.xmppDebug;
    }

    public void setXMPPServer(String x) {
        this.xmppServer = x;
    }

    public String getXMPPServer() {
        return this.xmppServer;
    }

    public void setXMPPPort(String x) {
        this.xmppPort = x;
    }

    public String getXMPPPort() {
        return this.xmppPort;
    }

    public void setXMPPProxy(String x) {
        this.xmppProxy = x;
    }

    public String getXMPPProxy() {
        return this.xmppProxy;
    }

    public void setXMPPClient(String x) {
        this.xmppClient = x;
    }

    public String getXMPPClient() {
        return this.xmppClient;
    }

    public void setXMPPPin(String x) {
        this.xmppPin = x;
    }

    public String getXMPPPin(){
        return this.xmppPin;
    }

    public void setXMPPUsername(String x) {
        this.xmppUsername = x;
    }

    public String getXMPPUsername() {
        return this.xmppUsername;
    }

    public void setXMPPPassword(String x) {
        this.xmppPassword = x;
    }

    public String getXMPPPassword() {
        return this.xmppPassword;
    }

    public void setXMPPDestination(String x) {
        this.xmppDestination = x;
    }

    public String getXMPPDestination() {
        return this.xmppDestination;
    }

    // threadpool
    public void setNthread(int x) {
        this.nthreads = x;
    }

    public int getNthread() {
        return this.nthreads;
    }

    public void setChunk(int x) {
        this.chunk  = x;
    }

    public int getChunk() {
        return this.chunk;
    }

    public void setConnectionString(String con) {
        this.connectionString = con;
    }

    public void setConnectionDriver(String drv) {
        this.connectionDriver = drv;
    }

    public void setConnectionUser(String user ) {
        this.connectionUser = user;
    }

    public void setConnectionPass(String pass)  {
        this.connectionPass = pass;
    }
    
    public String getConnectionString() {
        return this.connectionString;
    }

    public String getConnectionDriver() {
        return this.connectionDriver;
    }

    public String getConnectionUser() {
        return this.connectionUser;
    }

    public String getConnectionPass()  {
        return this.connectionPass;
    }
    
    public String getHost() {
        return this.host;
    }

    public void setHost(String x)  {
        this.host = x;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getInquiry() {
        return inquiry;
    }

    public void setInquiry(String inquiry) {
        this.inquiry = inquiry;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }
       
    public Connection getConnection() {
        Connection con = null;
        try {

              Class.forName(this.getConnectionDriver());
              con = DriverManager.getConnection(this.getConnectionString(), this.getConnectionUser(), this.getConnectionPass());

              //System.out.println("Connection ok.");

              return con;

        } catch (Exception e) {
            System.err.println("Exception @getConnection Setting: "+e.getMessage());
            logger.log(Level.FATAL,e.getMessage());
        }

        return con;
    }

    public void setConnections() {
        try {
            File f = new File("setting.properties");
            if (f.exists()) {
                Properties pro = new Properties();
                FileInputStream in = new FileInputStream(f);
                pro.load(in);
                //System.out.println("All key are given: " + pro.keySet());
                String conStr = pro.getProperty("Application.database.url");
                String conDrv = pro.getProperty("Application.database.driver");
                String conUser = pro.getProperty("Application.database.user");
                String conPass = pro.getProperty("Application.database.pass");
                String nthread = pro.getProperty("Application.nthread");
                String nchunk = pro.getProperty("Application.chunk");

                setConnectionString(conStr);
                setConnectionDriver(conDrv);
                setConnectionUser(conUser);
                setConnectionPass(conPass);
                setNthread(Integer.parseInt(nthread));
                setChunk(Integer.parseInt(nchunk));

                String x_server = pro.getProperty("Pelangi.xmpp.server");
                String x_port = pro.getProperty("Pelangi.xmpp.port");
                String x_proxy = pro.getProperty("Pelangi.xmpp.proxy");
                String x_account = pro.getProperty("Pelangi.xmpp.account");
                String x_pin = pro.getProperty("Pelangi.xmpp.pin");
                String x_debug = pro.getProperty("Pelangi.xmpp.debug");

                setXMPPServer(x_server);
                setXMPPPort(x_port);
                setXMPPProxy(x_proxy);
                setXMPPClient(x_account);
                setXMPPPin(x_pin);
                setXMPPDebug(x_debug);
                
                String x_host = pro.getProperty("addon.host");
                String x_username = pro.getProperty("addon.username");
                String x_password = pro.getProperty("addon.password");
                String x_secretKey = pro.getProperty("addon.secret_key");
                
                setHost(x_host);
                setUsername(x_username);
                setPassword(x_password);
                setSecretKey(x_secretKey);
                
                String x_inquiry = pro.getProperty("addon.inquiry");
                String x_payment = pro.getProperty("addon.payment");
                String x_advice = pro.getProperty("addon.advice");
                
                setInquiry(x_inquiry);
                setPayment(x_payment);
                setAdvice(x_advice);
                
                Map<String, String> persistenceMap = new HashMap<String, String>();
                persistenceMap.put("openjpa.ConnectionURL", pro.getProperty("persistence.url"));
                persistenceMap.put("openjpa.ConnectionUserName", pro.getProperty("persistence.user"));
                persistenceMap.put("openjpa.ConnectionPassword", pro.getProperty("persistence.pass"));
                persistenceMap.put("openjpa.ConnectionDriverName", pro.getProperty("persistence.driver"));
                setPersistenceMap(persistenceMap);
                
                String x_modName = pro.getProperty("Application.name");
                setAppName(x_modName);
                String x_modTimeout = pro.getProperty("Application.timeout");
                setAppTimeout(x_modTimeout);               
            }
            else {
                System.out.println("File setting not found");
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e);
        }
    }

}