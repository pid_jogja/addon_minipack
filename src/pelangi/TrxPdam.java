package pelangi;

import static com.sun.net.ssl.internal.ssl.Provider.install;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import model.TransactionDetails;
import model.Transactions;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jivesoftware.smack.Chat;
import org.json.JSONObject;
import sun.net.www.protocol.http.HttpURLConnection;

public class TrxPdam implements Runnable {

    private Thread t;
    private String threadName;
    private String pesan;
    private String client_target;
    private Logger logger = null;

    String pengirim[] = new String[2];

    int inbox_id = 0;
    String rc = "";
    String rc_desc = "";
    String pin = "";
    String trx_id = "";
    String trx_type = "";
    String produk = "";
    String hp = "";
    String idpel = "";
    String nominal = "0";
    String host_addon = "";
    int timewait = 0;

    Chat chat;

    Settings setting = new Settings();

    public TrxPdam(String threadNamex) {
        this.threadName = threadNamex;
    }

    public TrxPdam(String threadNamex, String pesanx) {
        this.threadName = threadNamex;
        this.pesan = pesanx;
    }

    public TrxPdam(String threadNamex, String pesanx, Chat chatx) {
        this.threadName = threadNamex;
        this.pesan = pesanx;
        this.chat = chatx;
    }

    public TrxPdam(String threadNamex, String pesanx, Chat chatx, String clientx) {
        this.threadName = threadNamex;
        this.pesan = pesanx;
        this.chat = chatx;
        this.client_target = clientx;
    }

    public TrxPdam(String threadNamex, String pesanx, Chat chatx, String clientx, String host, int timewait) {
        // parsing trx_id from oto
        Pattern pola = Pattern.compile("R#(\\w*)[\\s]*(\\w*).(\\w*).(\\w*).(\\w*)", Pattern.DOTALL);
        Matcher matcher = pola.matcher(pesanx);
        
        String x_id = "";
        if (matcher.matches()) {
            x_id = matcher.group(1);
        }
        
        logger = Logger.getLogger(TrxPdam.class + "[" + x_id + "]");
        
        this.threadName = threadNamex;
        logger.log(Level.INFO, "Thread Name : " + threadName);
        this.pesan = pesanx;
        logger.log(Level.INFO, "Pesan : " + pesan);
        this.chat = chatx;
        this.client_target = clientx;
        logger.log(Level.INFO, "Client target : " + client_target);
        this.host_addon = host;
        logger.log(Level.INFO, "Host : " + host_addon);
        this.timewait = timewait;
        logger.log(Level.INFO, "Time wait : " + timewait);

        setting.setConnections();
    }

    public void start() {
        logger.log(Level.INFO, "Starting " + threadName);
        if (t == null) {
            t = new Thread(this, threadName);
            t.start();
        }
    }

    @Override
    public void run() {

        long transaction_id = 0;
        
        try {
            pengirim[0] = "";
            pengirim[1] = "";

            // parse request
            Pattern pola = Pattern.compile("R#(\\w*)[\\s]*(\\w*).(\\w*).(\\w*).(\\w*)", Pattern.DOTALL);
            Matcher matcher = pola.matcher(pesan);
            logger.log(Level.INFO, "regex matching : " + matcher.matches());

            if (matcher.matches()) {
                trx_id = matcher.group(1);
                logger.log(Level.INFO, "trx_id : " + trx_id);
                trx_type = matcher.group(2);
                logger.log(Level.INFO, "type : " + trx_type);
                produk = matcher.group(3);
                logger.log(Level.INFO, "produk : " + produk);
                idpel = matcher.group(4);
                logger.log(Level.INFO, "idpel : " + idpel);

                ///////check duplikasi//////////
                long id_duplikat = isDuplicateTrxId(trx_id);

                if (id_duplikat == 0) {
                    ///////insert into transactions///////
                    transaction_id = initialTrx(trx_id, idpel, produk, trx_type, "200", pesan);
                    
                    String prodCode = "";
                    String trxTypePlg = "";
                    
                    if (!produk.equalsIgnoreCase("")) {
                        prodCode = getProductCode(produk);
                        logger.log(Level.INFO, "product_code : " + prodCode);
                    }
                                                            
                    if (prodCode.equalsIgnoreCase("") || prodCode.equalsIgnoreCase("0")) {
                        logger.log(Level.INFO, "unknown product " + produk);
                        
                        try {
                            String tgl = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date());
                            String formatOto = pesan + " UNKNOWN PRODUCT. Tanggal " + tgl;
                            logger.info("Message to send : " + formatOto);
                            chat.sendMessage(formatOto);
                        } catch (Exception e) {
                            logger.log(Level.FATAL, e.toString());
                        }
                    } else {
                        // hit api to purchase
                        Settings stg = new Settings();
                        stg.setConnections();

                        String username = stg.getUsername();
                        String password = stg.getPassword();
                        String secretKey = stg.getSecretKey();
                        
                        if (trx_type.toLowerCase().equalsIgnoreCase("cek") || trx_type.toLowerCase().equalsIgnoreCase("inq")) {
                            trxTypePlg = stg.getInquiry();
                            logger.log(Level.INFO, "trx_type : " + trxTypePlg);
                        } else if (trx_type.toLowerCase().equalsIgnoreCase("byr") || trx_type.toLowerCase().equalsIgnoreCase("pay")) {
                            trxTypePlg = stg.getPayment();
                            logger.log(Level.INFO, "trx_type : " + trxTypePlg);
                        }

                        // transaksi
                        logger.log(Level.INFO, "Processing trx " + trx_id + "#" + idpel + "#" + produk);

                        // multithread
                        trx(trx_id, trxTypePlg, idpel, produk, prodCode, pesan, this.host_addon, username, password, secretKey, timewait, transaction_id);
                    }
                } else {
                    // duplikat get last response
                    String lastResponse = getLastResponse(id_duplikat);
                    boolean advicePay = false;
                    
                    // parsing
                    String oto_message = "";
                    String message = "";
                    String rc = "";
                    
                    if (trx_type.toLowerCase().equalsIgnoreCase("cek") || trx_type.toLowerCase().equalsIgnoreCase("inq")) {
                        // INQ
                        String[] response = parseInqResponse(lastResponse);
                        rc = response[0];
                        
                        if (rc.equalsIgnoreCase("00")) {
                            advicePay = false;
                            message = "CEK TAGIHAN PDAM BERHASIL";
                            oto_message = message + ". IDPEL: " + response[1].trim() + "/" +
                                "NAMA: " + response[2].trim() + "/" +
                                "JMLLBR: " + response[3].trim() + "/" +
                                "BLTH: " + response[4].trim() + "/" +
                                "RPTAG: " + Long.parseLong(response[5].trim()) + "/" +
                                "ADMCHARGE: " + Long.parseLong(response[6].trim()) + "/" +
                                "TOTAL: " + Long.parseLong(response[7].trim());
                            
                            logger.info("OTO MSG : " + oto_message);
                        } else if (rc.equalsIgnoreCase("0001") || rc.equalsIgnoreCase("0007")) {
                            advicePay = true;
                            message = "CEK TAGIHAN PDAM PENDING";
//                            oto_message = message + ". RC : " + rc + " - " + response[1].trim();
                            oto_message = message + ". RC: " + rc + "/DESC: PENDING";
                            
                            logger.info("OTO MSG : " + oto_message);
                        } else {
                            advicePay = false;
                            message = "CEK TAGIHAN PDAM GAGAL";
                            oto_message = message + ". RC: " + rc + "/DESC: " + response[1].trim().toUpperCase();
                            
                            logger.info("OTO MSG : " + oto_message);
                        }
                    } else if (trx_type.toLowerCase().equalsIgnoreCase("byr") || trx_type.toLowerCase().equalsIgnoreCase("pay")) {
                        // PAY
                        String[] response = parsePayResponse(lastResponse);
                        rc = response[0];
                        
                        if (rc.equalsIgnoreCase("00")) {
                            advicePay = false;
                            
                            // GET FEE
                            String prodFee = getProductFee(produk);
                            Long fee = Long.parseLong(prodFee.trim());
                            logger.info("Product fee : " + fee);

                            Long amount = Long.parseLong(response[7].trim());
                            logger.info("Amount from plg : " + amount);

                            Long price = amount - fee;
                            logger.info("Price from plg : " + price);
                            
                            message = "BAYAR TAGIHAN PDAM BERHASIL";
                            oto_message = message + ". IDPEL: " + response[1].trim() + "/" +
                                "NAMA: " + response[2].trim() + "/" +
                                "NOREFF: " + response[8].trim() + "/" +
                                "JMLLBR: " + response[3].trim() + "/" +
                                "BLTH: " + response[4].trim() + "/" +
                                "RPTAG: " + Long.parseLong(response[5].trim()) + "/" +
                                "ADMCHARGE: " + Long.parseLong(response[6].trim()) + "/" +
                                "TOTAL: " + amount + "/" +
                                "HARGA: " + price;
                            
                            logger.info("OTO MSG : " + oto_message);
                        } else if (rc.equalsIgnoreCase("0001") || rc.equalsIgnoreCase("0007")) {
                            advicePay = true;
                            message = "BAYAR TAGIHAN PDAM PENDING";
//                            oto_message = message + ". RC: " + rc + "/DESC: " + response[1].trim();
                            oto_message = message + ". RC: " + rc + "/DESC: PENDING";
                            
                            logger.info("OTO MSG : " + oto_message);
                        } else {
                            advicePay = false;
                            message = "BAYAR TAGIHAN PDAM GAGAL";
                            oto_message = message + ". RC: " + rc + "/DESC: " + response[1].trim().toUpperCase();
                            
                            logger.info("OTO MSG : " + oto_message);
                        }
                    }
                    
                    if (advicePay) {
                        try {
                            String prodCode = "";
                            String trxTypePlg = "";

                            if (!produk.equalsIgnoreCase("")) {
                                prodCode = getProductCode(produk);
                                logger.log(Level.INFO, "product_code : " + prodCode);
                            }

                            if (prodCode.equalsIgnoreCase("") || prodCode.equalsIgnoreCase("0")) {
                                logger.log(Level.INFO, "unknown product " + produk);

                                try {
                                    String tgl = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date());
                                    String formatOto = pesan + " PRODUK SALAH . Tanggal " + tgl;
                                    logger.info("Message to send : " + formatOto);
                                    chat.sendMessage(formatOto);
                                } catch (Exception e) {
                                    logger.log(Level.FATAL, e.toString());
                                }
                            } else {
                                // hit api to purchase
                                Settings stg = new Settings();
                                stg.setConnections();

                                String username = stg.getUsername();
                                String password = stg.getPassword();
                                String secretKey = stg.getSecretKey();

                                if (trx_type.toLowerCase().equalsIgnoreCase("cek") || trx_type.toLowerCase().equalsIgnoreCase("inq")) {
                                    trxTypePlg = stg.getInquiry();
                                    logger.log(Level.INFO, "trx_type : " + trxTypePlg);
                                } else if (trx_type.toLowerCase().equalsIgnoreCase("byr") || trx_type.toLowerCase().equalsIgnoreCase("pay")) {
                                    trxTypePlg = stg.getPayment();
                                    logger.log(Level.INFO, "trx_type : " + trxTypePlg);
                                }
                                
                                // advice
                                logger.log(Level.INFO, "Processing advice " + trx_id + "#" + idpel + "#" + produk);

                                // hit advice
                                advice(trx_id, trxTypePlg, idpel, produk, prodCode, pesan, this.host_addon, username, password, secretKey, timewait, id_duplikat);
                            }
                        } catch (Exception e) {
                            logger.log(Level.FATAL, e.toString());

                            String tgl = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                            String formatOto = pesan + " SYSTEM ERROR. Tanggal " + tgl;
                            logger.info("Message to send : " + formatOto);

                            try {
                                chat.sendMessage(formatOto);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                logger.log(Level.FATAL, ex.toString());
                            }
                        }
                    } else {
                        String trxStatus = lastTrxStatus(trx_id);
                        String tgl = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date());
                        String formatOto = pesan + " " + oto_message + ". Tanggal " + tgl;
                        logger.info("Message to send : " + formatOto);

                        // Kirim pesan jika ok
                        chat.sendMessage(formatOto);
                        
                        // Jika statusnya system error maka update jawaban provider
                        if (trxStatus.equalsIgnoreCase("500")) {
                            updateTrxData(transaction_id, oto_message, rc);
                        }
                    }
                }
            } else {
                try {
                    String tgl = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date());
                    String formatOto = pesan + " FORMAT SALAH. Tanggal " + tgl;
                    logger.info("Message to send : " + formatOto);
                    chat.sendMessage(formatOto);
                } catch (Exception e) {
                    logger.log(Level.FATAL, e.toString());
                }
            }

        } catch (Exception err) {
            logger.info("Error : " + err.toString());
            
            try {
                String tgl = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date());
                String formatOto = pesan + " KODE PRODUK SALAH. Tanggal " + tgl;
                            
                logger.info("Message to send : " + formatOto);
                chat.sendMessage(formatOto);

                updateTrxData(transaction_id, formatOto, "205");
            } catch (Exception e) {
                logger.log(Level.FATAL, e.toString());
            }
        }
    }
    
    public String[] parseInqResponse(String response) {
        String[] xdata = new String[8];
        /*
         * 0: rc
         * 1: idpel
         * 2: nama
         * 3: lembar
         * 4: blth
         * 5: rptag
         * 6: admin
         * 7: total
        */
        
        JSONObject json = new JSONObject(response);
        
        logger.info("Parsing inquiry response...");
        
        if (json != null) {
            JSONObject data = json.getJSONObject("data");
            JSONObject trx = data.getJSONObject("trx");
            
            // get rc
            try {
                // RC
                xdata[0] = trx.getString("rc");
                logger.info("rc : " + xdata[0]);
                
                if (xdata[0].equalsIgnoreCase("00")) {
                    // IDPEL
                    xdata[1] = trx.getString("idpel");
                    logger.info("idpel : " + xdata[1]);
                    // NAMA
                    xdata[2] = trx.getString("name");
                    logger.info("name : " + xdata[2]);
                    // LEMBAR
                    xdata[3] = trx.getString("bill_count");
                    logger.info("bill_count : " + xdata[3]);
                    // BLTH
                    xdata[4] = trx.getString("blth");
                    logger.info("blth : " + xdata[4]);
                    // RPTAG
                    xdata[5] = trx.getString("rp_tag");
                    logger.info("rp_tag : " + xdata[5]);
                    // ADMIN
                    xdata[6] = trx.getString("admin_charge");
                    logger.info("admin_charge : " + xdata[6]);
                    // TOTAL
                    xdata[7] = trx.getString("amount");
                    logger.info("amount : " + xdata[7]);
                } else {
                    xdata[1] = trx.getString("desc");
                    logger.info("desc : " + xdata[1]);
                }
            } catch (Exception e) {
                logger.log(Level.FATAL, e.toString());
                
                try {
                    JSONObject objRc = trx.getJSONObject("rc");
                    // RC
                    xdata[0] = objRc.getString("0");
                    logger.info("rc : " + xdata[0]);
                    // DESC
                    xdata[1] = trx.getString("desc");
                    logger.info("desc : " + xdata[1]);
                } catch (Exception ex) {
                    logger.log(Level.FATAL, ex.toString());
                    xdata[0] = "0002"; /// ZERO TWO
                    xdata[1] = "Gagal parsing data JSON";
                }
            }
        }
        
        return xdata;
    }
    
    public String[] parsePayResponse(String response) {
        String[] xdata = new String[9];
        /*
         * 0: rc
         * 1: idpel
         * 2: nama
         * 3: lembar
         * 4: blth
         * 5: rptag
         * 6: admin
         * 7: total
         * 8: noreff
        */
        
        JSONObject json = new JSONObject(response);
        
        logger.info("Parsing payment response...");
        
        if (json != null) {
            JSONObject data = json.getJSONObject("data");
            JSONObject trx = data.getJSONObject("trx");
            
            // get rc
            try {
                // RC
                xdata[0] = trx.getString("rc");
                logger.info("rc : " + xdata[0]);
                
                if (xdata[0].equalsIgnoreCase("00")) {
                    // IDPEL
                    xdata[1] = trx.getString("idpel");
                    logger.info("idpel : " + xdata[1]);
                    // NAMA
                    xdata[2] = trx.getString("name");
                    logger.info("name : " + xdata[2]);
                    // LEMBAR
                    xdata[3] = trx.getString("bill_count");
                    logger.info("bill_count : " + xdata[3]);
                    // BLTH
                    xdata[4] = trx.getString("blth");
                    logger.info("blth : " + xdata[4]);
                    // RPTAG
                    xdata[5] = trx.getString("rp_tag");
                    logger.info("rp_tag : " + xdata[5]);
                    // ADMIN
                    xdata[6] = trx.getString("admin_charge");
                    logger.info("admin_charge : " + xdata[6]);
                    // TOTAL
                    xdata[7] = trx.getString("amount");
                    logger.info("amount : " + xdata[7]);
                    // NOREFF
                    xdata[8] = trx.getString("switching_ref");
                    logger.info("switching_ref : " + xdata[8]);
                } else {
                    xdata[1] = trx.getString("desc");
                    logger.info("desc : " + xdata[1]);
                }
            } catch (Exception e) {
                logger.log(Level.FATAL, e.toString());
                logger.log(Level.INFO, "RC nya ngobjek cuy!");
                
                try {
                    JSONObject objRc = trx.getJSONObject("rc");
                    // RC
                    xdata[0] = objRc.getString("0");
                    logger.info("rc : " + xdata[0]);
                    // DESC
                    xdata[1] = trx.getString("desc");
                    logger.info("desc : " + xdata[1]);
                } catch (Exception ex) {
                    logger.log(Level.FATAL, ex.toString());
                    xdata[0] = "0002"; /// ZERO TWO
                    xdata[1] = "Gagal parsing data JSON";
                }
            }
        }
        
        return xdata;
    }
    
    public String getProductCode(String product) {
        String prodCode = "";
        logger.log(Level.INFO, "Getting product code... " + product);
        
        try {
            File f = new File("product.properties");

            if (f.exists()) {
                Properties pro = new Properties();
                FileInputStream in = new FileInputStream(f);
                pro.load(in);

                prodCode = pro.getProperty("product." + product);
            } else {
                logger.log(Level.INFO, "File setting not found");
                
                prodCode = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.ERROR, e.getMessage());
            
            prodCode = "0";
        }
        
        return prodCode;
    }
    
    public String getProductFee(String product) {
        String prodFee = "";
        logger.log(Level.INFO, "Getting product fee... " + product);
        
        try {
            File f = new File("fee.properties");

            if (f.exists()) {
                Properties pro = new Properties();
                FileInputStream in = new FileInputStream(f);
                pro.load(in);

                prodFee = pro.getProperty("fee." + product);
            } else {
                logger.log(Level.INFO, "File setting not found");
                
                prodFee = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.ERROR, e.getMessage());
            
            prodFee = "0";
        }
        
        return prodFee;
    }

    public String transaksi(String trx_id, String trx_type, String idpel, String prod_code, 
            String api_url, String username, String password, String secret_key, int timewait, long transaction_id) {
        
        String fin_result = "";
        Date responseDate = new Date();
        Date requestDate = new Date();
        String strRequest = null;
        
        try {
            URL obj = new URL(api_url);

            // If you use http protocol
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // If you use https protocol
//            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
//            SSLSocketFactory sslSocketFactory = createTrustAllSslSocketFactory();
//            con.setSSLSocketFactory(sslSocketFactory);
//            HostnameVerifier host = createHostName();
//            con.setHostnameVerifier(host);
            // Comment above code if you do not use https protocol

            con.setConnectTimeout(Integer.parseInt(setting.getAppTimeout()));
            con.setReadTimeout(Integer.parseInt(setting.getAppTimeout())); 
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");	
            String trx_date = sdf.format(new Date());
    		
            String signature = convertMD5(username + password + prod_code + trx_date + secret_key);
            logger.log(Level.INFO, "Signature : " + signature);

            // System.out.println(trx_date);
            // System.out.println(signature);
            logger.log(Level.INFO, "Header : PELANGIREST username=a"+username+"&password="+password+"&signature="+signature);

            String urlParameters =
                "trx_date="+trx_date+
                "&trx_type="+trx_type+
                "&trx_id="+trx_id+
                "&cust_msisdn="+
                "&cust_account_no="+idpel+
                "&product_id="+prod_code+
                "&product_nomination="+
                "&periode_payment=";

            // add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Authorization", "PELANGIREST username="+username+"&password="+password+"&signature="+signature);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
            con.setRequestProperty("Content-Language", "en-US");

            logger.log(Level.INFO, "Sending 'POST' request to URL : " + api_url);
            logger.log(Level.INFO, "Post parameters : " + urlParameters);
            
            strRequest = urlParameters;
            
            requestDate = new Date();

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
            
            int responseCode = con.getResponseCode();
            logger.log(Level.INFO, "Response code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();	

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            
            responseDate = new Date();

            in.close();
            fin_result = response.toString();
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.ERROR, "ERROR HIT : " + e.toString());
            
            fin_result = "{" +
                "\"data\": {" +
                "\"trx\": {" +
                "\"trx_id\": \"" + trx_id + "\"," +
                "\"rc\": {" +
                "\"0\": \"0007\"" +
                "}," +
                "\"desc\": \"Gagal hit ke server API\"" +
                "}" +
                "}" +
                "}";
        }
        
        responseDate = new Date();

        //////////SAVE TO DB///////            
        insertTrxDetail(transaction_id, strRequest, requestDate, fin_result, responseDate);
        //////////SAVE TO DB////////

        return fin_result;
    }

    public void trx(String trx_id, String trx_type, String idpel, String produk, String prod_code, String msg, String host, String username, String password, String secret_key, int timewait, long transaction_id) {
        logger.info("Running...");
        logger.info("trx_type : " + trx_type);

        String hasil = transaksi(trx_id, trx_type, idpel, prod_code, host, username, password, secret_key, timewait, transaction_id);
        logger.info("RESULT : " + hasil);
        
        try {
            logger.info("Acknowledge");
            
            Settings stg = new Settings();
            stg.setConnections();

            String inqTypePlg = stg.getInquiry();
            String payTypePlg = stg.getPayment();
                        
            // String responseBody;
            String[] target = client_target.split("/");

            String xhasil = new String(hasil);

            // parsing
            String oto_message = "";
            String message = "";
            String rc = "";

            if (trx_type.equalsIgnoreCase(inqTypePlg)) {
                // INQ
                String[] response = parseInqResponse(xhasil);
                rc = response[0];

                if (rc.equalsIgnoreCase("00")) {
                    message = "CEK TAGIHAN PDAM BERHASIL";
                    oto_message = message + ". IDPEL: " + response[1].trim() + "/" +
                        "NAMA: " + response[2].trim() + "/" +
                        "JMLLBR: " + response[3].trim() + "/" +
                        "BLTH: " + response[4].trim() + "/" +
                        "RPTAG: " + Long.parseLong(response[5].trim()) + "/" +
                        "ADMCHARGE: " + Long.parseLong(response[6].trim()) + "/" +
                        "TOTAL: " + Long.parseLong(response[7].trim());
                    
                    logger.log(Level.INFO, "OTO MSG : " + oto_message);
                } else if (rc.equalsIgnoreCase("0001")) {
                    message = "CEK TAGIHAN PDAM PENDING";
//                    oto_message = message + ". RC: " + rc + "/DESC: " + response[1].trim();
                    oto_message = message + ". RC: " + rc + "/DESC: PENDING";
                    
                    logger.log(Level.INFO, "OTO MSG : " + oto_message);
                } else {
                    message = "CEK TAGIHAN PDAM GAGAL";
                    oto_message = message + ". RC: " + rc + "/DESC: " + response[1].trim().toUpperCase();
                    
                    logger.log(Level.INFO, "OTO MSG : " + oto_message);
                }
            } else if (trx_type.equalsIgnoreCase(payTypePlg)) {              
                // PAY
                String[] response = parsePayResponse(xhasil);
                rc = response[0];
                
                if (rc.equalsIgnoreCase("00")) {
                    // GET FEE
                    String prodFee = getProductFee(produk);
                    Long fee = Long.parseLong(prodFee.trim());
                    logger.info("Product fee : " + fee);

                    Long amount = Long.parseLong(response[7].trim());
                    logger.info("Amount from plg : " + amount);

                    Long price = amount - fee;
                    logger.info("Price from plg : " + price);
                    
                    message = "BAYAR TAGIHAN PDAM BERHASIL";
                    oto_message = message + ". IDPEL: " + response[1].trim() + "/" +
                        "NAMA: " + response[2].trim() + "/" +
                        "NOREFF: " + response[8].trim() + "/" +
                        "JMLLBR: " + response[3].trim() + "/" +
                        "BLTH: " + response[4].trim() + "/" +
                        "RPTAG: " + Long.parseLong(response[5].trim()) + "/" +
                        "ADMCHARGE: " + Long.parseLong(response[6].trim()) + "/" +
                        "TOTAL: " + amount + "/" +
                        "HARGA: " + price;
                    
                    logger.log(Level.INFO, "OTO MSG : " + oto_message);
                } else if (rc.equalsIgnoreCase("0001")) {
                    message = "BAYAR TAGIHAN PDAM PENDING";
//                    oto_message = message + ". RC: " + rc + "/DESC: " + response[1].trim();
                    oto_message = message + ". RC: " + rc + "/DESC: PENDING";
                    
                    logger.log(Level.INFO, "OTO MSG : " + oto_message);
                } else {
                    message = "BAYAR TAGIHAN PDAM GAGAL";
                    oto_message = message + ". RC: " + rc + "/DESC: " + response[1].trim().toUpperCase();
                    
                    logger.log(Level.INFO, "OTO MSG : " + oto_message);
                }
            }

            String tgl = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            String formatOto = pesan + " " + oto_message + ". Tanggal " + tgl;
            
            logger.log(Level.INFO, "Message to send : " + formatOto);
            chat.sendMessage(formatOto);
            
            updateTrxData(transaction_id, formatOto, rc);
        } catch (Exception e) {
            logger.log(Level.FATAL, e.toString());
            
            String tgl = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            String formatOto = pesan + " SYSTEM ERROR. Tanggal " + tgl;
            
            logger.log(Level.INFO, "Message to send : " + formatOto);

            try {
                chat.sendMessage(formatOto);
                updateTrxData(transaction_id, formatOto, "500");
            } catch (Exception er) {
                e.printStackTrace();
                logger.log(Level.FATAL, e.toString());
            }
        }

    }
    
    public void advice(String trx_id, String trx_type, String idpel, String produk, String prod_code, String msg, String host, String username, String password, String secret_key, int timewait, long transaction_id) {
        logger.info("Advice -> Running...");
        logger.info("trx_type : " + trx_type);

        String hasil = transaksi(trx_id, trx_type, idpel, prod_code, host, username, password, secret_key, timewait, transaction_id);
        logger.info("RESULT : " + hasil);
        
        try {
            logger.info("Acknowledge");
            
            Settings stg = new Settings();
            stg.setConnections();

            String inqTypePlg = stg.getInquiry();
            String payTypePlg = stg.getPayment();
                        
            // String responseBody;
            String[] target = client_target.split("/");

            String xhasil = new String(hasil);

            // parsing
            String oto_message = "";
            String message = "";
            String rc = "";

            if (trx_type.equalsIgnoreCase(inqTypePlg)) {
                // INQ
                String[] response = parseInqResponse(xhasil);
                rc = response[0];

                if (rc.equalsIgnoreCase("00")) {
                    message = "CEK TAGIHAN PDAM BERHASIL";
                    oto_message = message + ". IDPEL: " + response[1].trim() + "/" +
                        "NAMA: " + response[2].trim() + "/" +
                        "JMLLBR: " + response[3].trim() + "/" +
                        "BLTH: " + response[4].trim() + "/" +
                        "RPTAG: " + Long.parseLong(response[5].trim()) + "/" +
                        "ADMCHARGE: " + Long.parseLong(response[6].trim()) + "/" +
                        "TOTAL: " + Long.parseLong(response[7].trim());
                    
                    logger.log(Level.INFO, "OTO MSG : " + oto_message);
                } else if (rc.equalsIgnoreCase("0001")) {
                    message = "CEK TAGIHAN PDAM PENDING";
//                    oto_message = message + ". RC: " + rc + "/DESC: " + response[1].trim();
                    oto_message = message + ". RC: " + rc + "/DESC: PENDING";
                    
                    logger.log(Level.INFO, "OTO MSG : " + oto_message);
                } else {
                    message = "CEK TAGIHAN PDAM GAGAL";
                    oto_message = message + ". RC: " + rc + "/DESC: " + response[1].trim().toUpperCase();
                    
                    logger.log(Level.INFO, "OTO MSG : " + oto_message);
                }
            } else if (trx_type.equalsIgnoreCase(payTypePlg)) {              
                // PAY
                String[] response = parsePayResponse(xhasil);
                rc = response[0];
                
                if (rc.equalsIgnoreCase("00")) {
                    // GET FEE
                    String prodFee = getProductFee(produk);
                    Long fee = Long.parseLong(prodFee.trim());
                    logger.info("Product fee : " + fee);

                    Long amount = Long.parseLong(response[7].trim());
                    logger.info("Amount from plg : " + amount);

                    Long price = amount - fee;
                    logger.info("Price from plg : " + price);
                    
                    message = "BAYAR TAGIHAN PDAM BERHASIL";
                    oto_message = message + ". IDPEL: " + response[1].trim() + "/" +
                        "NAMA: " + response[2].trim() + "/" +
                        "NOREFF: " + response[8].trim() + "/" +
                        "JMLLBR: " + response[3].trim() + "/" +
                        "BLTH: " + response[4].trim() + "/" +
                        "RPTAG: " + Long.parseLong(response[5].trim()) + "/" +
                        "ADMCHARGE: " + Long.parseLong(response[6].trim()) + "/" +
                        "TOTAL: " + amount + "/" +
                        "HARGA: " + price;
                    
                    logger.log(Level.INFO, "OTO MSG : " + oto_message);
                } else if (rc.equalsIgnoreCase("0001")) {
                    message = "BAYAR TAGIHAN PDAM PENDING";
//                    oto_message = message + ". RC: " + rc + "/DESC: " + response[1].trim();
                    oto_message = message + ". RC: " + rc + "/DESC: PENDING";
                    
                    logger.log(Level.INFO, "OTO MSG : " + oto_message);
                } else {
                    message = "BAYAR TAGIHAN PDAM GAGAL";
                    oto_message = message + ". RC: " + rc + "/DESC: " + response[1].trim().toUpperCase();
                    
                    logger.log(Level.INFO, "OTO MSG : " + oto_message);
                }
            }

            String tgl = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            String formatOto = pesan + " " + oto_message + ". Tanggal " + tgl;
            
            logger.log(Level.INFO, "Message to send : " + formatOto);
            chat.sendMessage(formatOto);
            
            updateTrxData(transaction_id, formatOto, rc);
        } catch (Exception e) {
            logger.log(Level.FATAL, e.toString());
            
            String tgl = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            String formatOto = pesan + " SYSTEM ERROR. Tanggal " + tgl;
            
            logger.log(Level.INFO, "Message to send : " + formatOto);

            try {
                chat.sendMessage(formatOto);
                updateTrxData(transaction_id, formatOto, "500");
            } catch (Exception er) {
                e.printStackTrace();
                logger.log(Level.FATAL, e.toString());
            }
        }

    }
    
    public String hashToMD5(String signature) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] array = md.digest(signature.getBytes());
            StringBuffer sb = new StringBuffer();
            
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    public static String convertMD5(String x) throws NoSuchAlgorithmException {
        String result = "";
        // MessageDigest md = MessageDigest.getInstance("SHA-1");
        MessageDigest md = MessageDigest.getInstance("MD5");
        
        md.update(x.getBytes());
        byte byteData[] = md.digest();
 
        // convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
 
        result = sb.toString();
        return result;
    }
    
    private static SSLSocketFactory createTrustAllSslSocketFactory() throws Exception {
        TrustManager[] byPassTrustManagers = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(X509Certificate[] chain, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) {
            }
        }};
        
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, byPassTrustManagers, new SecureRandom());
        
        return sslContext.getSocketFactory();
    }  
    
    private static HostnameVerifier createHostName() throws Exception {
    	HostnameVerifier hv = new HostnameVerifier() {
            public boolean verify(String arg0, SSLSession arg1) {
                return true;
            }
        };

        return hv;
    }

    private static void install() throws Exception {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    // Trust always
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    // Trust always
                }
            }
        };

        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        // Create empty HostnameVerifier
        HostnameVerifier hv = new HostnameVerifier() {
            public boolean verify(String arg0, SSLSession arg1) {
                return true;
            }
        };

        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        HttpsURLConnection.setDefaultHostnameVerifier(hv);
    }

    public long initialTrx(String trx_id, String cust_number, String product_code, String trx_type, String trx_status, String mesage) {
        
        logger.log(Level.INFO, ":::INSERT TRX:::");
        
        long transaction_id = 0;

        System.out.println("Setting persistence : " + setting.getPersistenceMap().toString());

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ADDON", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        try {
            Transactions trx = new Transactions();
            em.getTransaction().begin();

            trx.setTrxId(trx_id);
            trx.setCustNumber(cust_number);
            trx.setProductCode(product_code);
            trx.setTrxType(trx_type);
            trx.setTrxStatus(trx_status);
            trx.setRequestMessage(mesage);
            trx.setRequestDatetime(new Date());
            trx.setModuleName(setting.getAppName());
            
            /// validate
//            validateConstraint(trx);

            em.persist(trx);
            em.flush();
            transaction_id = trx.getId();
            em.getTransaction().commit();

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        return transaction_id;
    }

    public void updateTrxData(Long transaction_id, String response_message, String trx_status) {

        logger.log(Level.INFO, ":::UPDATE TRX:::");

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ADDON", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        try {
            Transactions myTrx = em.find(Transactions.class, transaction_id);
            
            em.getTransaction().begin();
            
            myTrx.setTrxStatus(trx_status);
            myTrx.setResponseMessage(response_message);
            myTrx.setResponseDatetime(new Date());
            
            em.persist(myTrx);
            em.flush();

            em.getTransaction().commit();

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

    }

    public void insertTrxDetail(Long transaction_id, String request_data,
            Date request_datetime, String response_data, Date response_datetime) {

        logger.log(Level.INFO, ":::INSERT TRX DETAIL:::");

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ADDON", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        try {
            TransactionDetails trx = new TransactionDetails();
            em.getTransaction().begin();

            trx.setTransactionId(BigInteger.valueOf(transaction_id));
            trx.setRequestData(request_data);
            trx.setRequestDatetime(request_datetime);
            trx.setResponseData(response_data);
            trx.setResponseDatetime(response_datetime);
            
            em.persist(trx);
            em.flush();

            em.getTransaction().commit();

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

    }

    public long isDuplicateTrxId(String trx_id) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ADDON", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();
        
        long hasil = 0;
        logger.log(Level.INFO, "Cek duplikasi... " + trx_id);
        
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            // get current date time with Date()
            java.util.Date date = new java.util.Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

            String sql = "SELECT o FROM Transactions o "
                    + "WHERE o.trxId =: trx_id ORDER BY o.id DESC";

            Query query = em.createQuery(sql);
            query.setParameter("trx_id", trx_id);
            query.setMaxResults(1);

            for (Transactions m : (List<Transactions>) query.getResultList()) {
                hasil = m.getId().longValue();
                logger.log(Level.INFO, "Hasil cari duplikat trx : " + hasil);
            }

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        logger.log(Level.INFO, "Returning duplikasi : " + hasil);
        return hasil;
    }
    
    public String getLastResponse(long transaction_id) {

        logger.log(Level.INFO, "Duplicated transaction_id : " + transaction_id);

        String result = "";
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ADDON", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();
        try {

            String sql = "SELECT o FROM TransactionDetails o "
                    + " WHERE o.transactionId =: transaction_id ORDER BY o.id DESC";
            
            Query query = em.createQuery(sql);
            query.setParameter("transaction_id", transaction_id);
            query.setMaxResults(1);
            logger.log(Level.INFO, query.toString());

            for (TransactionDetails m : (List<TransactionDetails>) query.getResultList()) {
                logger.log(Level.INFO, "id : " + m.getId());

                result = m.getResponseData();
                logger.log(Level.INFO, "RESPONSE FROM DB : " + result);
            }

            em.close();
            factory.close();
        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
        } finally {

            if (em.isOpen()) {
                em.close();
            }
            if (factory.isOpen()) {
                factory.close();
            }

        }

        return result;
    }
    
    public String lastTrxStatus(String trx_id) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ADDON", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();
        
        String hasil = "0";
        logger.log(Level.INFO, "Cek duplikasi... " + trx_id);
        
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            // get current date time with Date()
            java.util.Date date = new java.util.Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

            String sql = "SELECT o FROM Transactions o "
                    + "WHERE o.trxId =: trx_id ORDER BY o.id DESC";

            Query query = em.createQuery(sql);
            query.setParameter("trx_id", trx_id);
            query.setMaxResults(1);

            for (Transactions m : (List<Transactions>) query.getResultList()) {
                hasil = m.getTrxStatus();
                logger.log(Level.INFO, "Hasil cek field trx_status : " + hasil);
            }

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        logger.log(Level.INFO, "Returning trx_status : " + hasil);
        return hasil;
    }
    
    public void validateConstraint(Transactions trx) {
        ValidatorFactory factoryx = Validation.buildDefaultValidatorFactory();
        Validator validator = factoryx.getValidator();

        Set<ConstraintViolation<Transactions>> constraintViolations = validator.validate(trx);

        if (constraintViolations.size() > 0 ) {
            System.out.println("[x] Constraint violations occurred.");

            for (ConstraintViolation<Transactions> contraints : constraintViolations) {
                System.out.println(contraints.getRootBeanClass().getSimpleName()+
                "." + contraints.getPropertyPath() + " " + contraints.getMessage());
            }
        }
    }

}
